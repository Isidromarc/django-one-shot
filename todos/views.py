from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def list_todos(request):
    list = TodoList.objects.all()
    context = {"todos": list}
    return render(request, "todos/list.html", context)


def todos_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {"todo": list}
    return render(request, "todos/detail.html", context)

def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm()

    context = {
        "form":form,
    }
    return render(request,"todos/newlist.html", context)

def edit_todo_list(request, id):
    list = get_object_or_404(TodoList,id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "form":form
    }
    return render(request, "todos/editlist.html", context)

def delete_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    context = {}
    return render(request,"todos/delete.html", context)

def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form":form
    }
    return render(request, "todos/newitem.html", context)

def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = item)
        if form.is_valid():
            edited_item = form.save()
            return redirect("todo_list_detail", edited_item.list.id)
    else:
        form = TodoItemForm(instance = item)
    context = {
        "form":form
    }
    return render(request,"todos/edititem.html", context)

def delete_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    list = item.list.id
    item.delete()
    return redirect("todo_list_detail", list)
