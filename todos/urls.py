from django.urls import path
from .views import (
    list_todos, todos_detail, create_todo_list, edit_todo_list,
    delete_todo_list, create_item, edit_item, delete_item)

urlpatterns = [
    path("", list_todos, name="todo_list_list"),
    path("<int:id>/", todos_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name="todo_item_update"),
    path("items/<int:id>/delete/", delete_item, name="todo_item_delete"),
]
